package cn.weixin.song.model;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cn.weixin.song.model.base.BaseMusicActivityPlayRecord;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.jcbase.core.model.Condition;
import com.jcbase.core.model.Operators;
import com.jcbase.core.util.CommonUtils;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class MusicActivityPlayRecord extends BaseMusicActivityPlayRecord<MusicActivityPlayRecord> {
	public static final MusicActivityPlayRecord dao = new MusicActivityPlayRecord();

	public MusicActivityPlayRecord getMusicActivityPlayRecord(int uid,int music_activity_id) {
		Set<Condition> conditions=Sets.newHashSet();
		conditions.add(new Condition("music_activity_id",Operators.EQ,music_activity_id));
		conditions.add(new Condition("uid",Operators.EQ,uid));
		return this.get(conditions);
	}

	public MusicActivityPlayRecord initMusicActivityPlayRecord(Integer uid,
			Integer music_activity_id) {
		MusicActivityPlayRecord musicActivityPlayRecord= new MusicActivityPlayRecord().set("uid", uid).set("last_status", 0)
				.set("current_level", 0)
		.set("music_activity_id", music_activity_id).set("play_date", new Date());
		musicActivityPlayRecord.save();
		return musicActivityPlayRecord;
	}

	public String getMusicActivityPlayRecordRanks(int music_activity_id){
		StringBuffer sb=new StringBuffer();
		Page<MusicActivityPlayRecord> page=getMusicActivityPlayRecordRanks(music_activity_id, 1, 10);
		int i=1;
		for(MusicActivityPlayRecord item : page.getList()){
			if(StrKit.notBlank(item.getStr("realname"))&&StrKit.notBlank(item.getStr("mobilephone"))){
				sb.append("第"+i+"名:"+item.getStr("realname").substring(0,1)+"**"+","+item.getStr("mobilephone").substring(0,3)+"****"+item.getStr("mobilephone").substring(7,11)+",得分:"+item.getInt("high_score")+",耗时:"+item.getLong("high_spend_time")+"毫秒\r\n");
				i++;
			}
		}
		return sb.toString();
	}
	
	
	public String getMyRank(Integer aid,int high_score,long high_spend_time){
		StringBuffer st=new  StringBuffer("你当前最高分数："+high_score+",耗时："+high_spend_time+"毫秒,排名第"+getRanking(aid, high_score, high_spend_time)+",打败了"+defeat(aid, high_score, high_spend_time)+"用户");
		return st.toString();
	}
	/**
	 * 根据分数及花费时间计算排名
	 */
	public long getRanking(Integer aid,int high_score,long high_spend_time) {
		String sql="select count(*) from `music_activity_play_record`  where high_score >? or (high_score=? and high_spend_time <?)";
		return Db.queryLong(sql, high_score,high_score,high_spend_time)+1;
	}
	/**
	 * 打败了多少的用户
	 */
	public String defeat(Integer aid,int high_score,long high_spend_time) {
		long allCount=this.getCount(CommonUtils.getConditions(new Condition("music_activity_id",Operators.EQ,aid)));
		long ranking=getRanking(aid, high_score, high_spend_time);
		if(ranking==1)return "100%";
		long defeat=(allCount-ranking)*100/allCount;
		return defeat+"%";
	}
	
	public Page<MusicActivityPlayRecord> getMusicActivityPlayRecordRanks(int music_activity_id,int pageNumber,int pageSize){
		String select="select u.realname,u.mobilephone,mapr.high_score,mapr.high_spend_time";
		String sqlExceptSelect="from music_activity_play_record mapr left join user u on mapr.uid=u.id where mapr.music_activity_id=? and u.realname is not null and u.mobilephone is not null order by mapr.high_score desc,mapr.high_spend_time asc";
		return MusicActivityPlayRecord.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect, music_activity_id);
	}
	
	public Page<Map<String,Object>> getMusicActivityPlayRecordRanksViews(int music_activity_id,int pageNumber,int pageSize){
		Page<MusicActivityPlayRecord> page=getMusicActivityPlayRecordRanks(music_activity_id, pageNumber, pageSize);
		List<Map<String,Object>> list=Lists.newArrayList();
		for(MusicActivityPlayRecord item : page.getList()){
			Map<String,Object> newItem=Maps.newHashMap();
			if(StrKit.notBlank(item.getStr("realname"))&&StrKit.notBlank(item.getStr("mobilephone"))){
				newItem.put("high_score", item.getInt("high_score"));
				newItem.put("high_spend_time", item.getLong("high_spend_time"));
				newItem.put("realname", item.getStr("realname").substring(0,1)+"**");
				newItem.put("mobilephone", item.getStr("mobilephone").substring(0,3)+"****"+item.getStr("mobilephone").substring(7,11));
				list.add(newItem);
			}
		}
		Page<Map<String,Object>> newPage=new Page<Map<String,Object>>(list, pageNumber, pageSize, page.getTotalPage(), page.getTotalRow());
		return newPage;
	}
}
