/**
 * Copyright (c) 2011-2016, Eason Pan(pylxyhome@vip.qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jcbase.core.controller;

import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.weixin.song.contant.R;

import com.jcbase.core.util.CommonUtils;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
/**
 * 
 * @author eason
 *
 */
public abstract class JCBaseController extends Controller {
	
	public final Log logger = LogFactory.getLog(getClass());
	public static final int BUFFER_SIZE = 1024 * 1024;
	/**
	 * 获取排序对象
	 * @author eason	
	 * @return
	 */
	protected LinkedHashMap<String,String> getOrderby(){
		String sord=this.getPara("sord");
		String sidx=this.getPara("sidx");
		LinkedHashMap<String,String> orderby=new LinkedHashMap<String,String>();
		if(CommonUtils.isNotEmpty(sidx)){
			orderby.put(sidx, sord);
		}
		return orderby;
	}
	/**
	 * 获取排序字符串
	 * @author eason	
	 * @return
	 */
	protected String getOrderbyStr(){
		String sord=this.getPara("sord");
		String sidx=this.getPara("sidx");
		if(CommonUtils.isNotEmpty(sidx)){
			return " order by "+ sidx+" "+sord;
		}
		return "";
	}
	/**
	 * 获取每几页
	 * @author eason	
	 * @return
	 */
	protected int getPage(){
		return this.getParaToInt("page", 1);
	}
	/**
	 * 获取每页数量
	 * @author eason	
	 * @return
	 */
	protected int getRows(){
		int rows=this.getParaToInt("rows", 20);
		if(rows>1000)rows=1000;
		return rows;
	}
	
	protected String getThisUrl(){
		StringBuffer url=this.getRequest().getRequestURL();
		if(StrKit.notBlank(this.getRequest().getQueryString())){
			url.append("?"+this.getRequest().getQueryString());
		}
		return url.toString();
	}
	
	/**
	 * 向客户端输出分页对象
	 * @author eason	
	 * @param pageInfo
	 */
	protected void renderPageInfo(Page pageInfo) {
		// TODO Auto-generated method stub
		LinkedHashMap<String,Object> mapData=new LinkedHashMap<String,Object>();
		mapData.put("code",0);
		mapData.put("data", pageInfo.getList());
		mapData.put("curPage", pageInfo.getPageNumber());
		mapData.put("totalCount", pageInfo.getTotalRow());
		mapData.put("totalPage", pageInfo.getTotalPage());
		this.renderJson(mapData);
	}
}





