package com.jcbase.core.util;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternUtils {

	private static final String digitFromStr="[0-9]+";
	
	private static final String doubleFromStr="[0-9]+\\.?[0-9]$";
	/**
	 * 2012-02-23
	 */
	private static final String dateFromStr="[0-9]{4}-[0-9]{2}-[0-9]{2}";
	public static int getDigitFromStr(String str){
		Pattern pattern = Pattern.compile(digitFromStr);
		Matcher matcher = pattern.matcher(str);
		int digit=0;
		if(matcher.find()){
			digit=Integer.valueOf(matcher.group(0));
		}
		return digit;
	}
	public static double getDoubleFromStr(String str){
		Pattern pattern = Pattern.compile(doubleFromStr);
		Matcher matcher = pattern.matcher(str);
		double digit=0;
		if(matcher.find()){
			digit=Double.valueOf(matcher.group(0));
		}
		return digit;
	}
	public static Date getDateFromStr(String str){
		Pattern pattern = Pattern.compile(dateFromStr);
		Matcher matcher = pattern.matcher(str);
		if(matcher.find()){
			String dateStr=matcher.group(0);
			return DateUtils.parseDate(dateStr, "yyyy-MM-dd");
		}
		return null;
	}
	public static boolean isQQ(String qq){
		String regEx = "^[1-9][0-9]{4,11}$";
		Pattern pat = Pattern.compile(regEx);  
		Matcher mat = pat.matcher(qq);  
		return mat.find();   
	}
	public static boolean isMobileNO(String mobileno){
		String regEx = "^[1][3,4,5,8,7][0-9]{9}$";
		Pattern pat = Pattern.compile(regEx);  
		Matcher mat = pat.matcher(mobileno);  
		return mat.find();   
	}
	public static void main(String[] args) {
		System.out.println("vip@1837877#1".split("vip@")[1]);
	}
}
